import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	static ArrayList<No> mapa;
	static No mapDepot;
	static int capacidade = 0;
	static int dimensao = 0;
	
	
	public static void main(String[] args) {

		/* Configura */
		Scanner scan = new Scanner(System.in);
		System.out.print("Insira o nome do arquivo: ");
		String arquivo = scan.nextLine();
		scan.close();
		
		long inicio = System.currentTimeMillis();
		carregaDoArquivo(arquivo);
		
		ArrayList<Cluster> clusters = new ArrayList<>(); 
		No depot = mapDepot; 
		int clusterid = -1;


		//constroi clusters
		while(loop()){ //loop checa se todos n�s foram visitados
			No vj = findFarthest(depot); 
			Cluster c = new Cluster(++clusterid, capacidade, depot);
			c.addNo(vj);
			clusters.add(c);
			while(true){ //adiciona n�s ao cluster enquanto capacidade > demanda
				No closest = findClosest(c.getGc());
				if(closest != null && c.getCapacidade() >= closest.getDemanda()){
					c.addNo(closest);
				}else{
					break;
				}
			}	
		}


		//ajusta clusters
		for (int i = 0; i < clusters.size(); i++) {
			ArrayList<No> nos = new ArrayList<No>(clusters.get(i).getCaminho());
			for (No vk : nos) {
				for (int j = 0; j < clusters.size(); j++) {
					//se no vk, do cluster i, esta mais perto do cluster j, e capacidade de j > demanda de vk, trocar n� de cluster
					if((i!=j) && (vk.distancia(clusters.get(j).getGc()) < vk.distancia(clusters.get(i).getGc()) ) && (clusters.get(j).getCapacidade() >= vk.getDemanda()) ){
						clusters.get(i).removeNo(vk);
						clusters.get(j).addNo(vk); 
					}
				}
			}
		}


		//usa simmulate dannealing pra achar melhor rota em cada cluster
		SimmulatedAnnealing sa = new SimmulatedAnnealing();
		for (Cluster cluster : clusters) {
			ArrayList<No> nos = new ArrayList<>();
			nos.add(depot);
			nos.addAll(cluster.getCaminho());
			nos = sa.resolve(nos);
			nos.remove(0);
			cluster.setCaminho(nos);
		}

		//custo e tempo de execu��o
		
		int custo = 0;
		for (Cluster cluster : clusters) {
			custo+=cluster.getCusto();
			cluster.print();
		}
		System.out.println("Custo: " + custo);
		long tempo = (System.currentTimeMillis() - inicio);
		System.out.println("Tempo: " + tempo);


	}

	private static void carregaDoArquivo(String arq) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(arq));
			br.readLine();
			br.readLine();
			br.readLine();
			String[] str = br.readLine().split(" ");
			dimensao = Integer.parseInt(str[2]); // Dimension
			br.readLine();
			str = br.readLine().split(" ");
			capacidade = Integer.parseInt(str[2]); // Capacity
			br.readLine();
			mapa = new ArrayList<>();
			for (int i = 0; i < dimensao; i++) {
				str = br.readLine().trim().split(" ");
				mapa.add(new No(i, Integer.parseInt(str[1]), Integer.parseInt(str[2]), 0));// Node
			}
			br.readLine();
			for (int i = 0; i < dimensao; i++) {
				str = br.readLine().trim().split(" ");
				mapa.get(Integer.parseInt(str[0]) - 1).setDemanda(Integer.parseInt(str[1]));// Demand
			}
			br.readLine();
			mapDepot = mapa.get(Integer.parseInt(br.readLine().trim()) - 1); 
			mapDepot.setVisitado(true);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	//acha n� mais longe do depot pra come�ar cluster
	private static No findFarthest(No dep){
		int distancia = 0;
		No farthest = null;
		for (No no : mapa) {
			if(!no.isVisitado()){
				if(dep.distancia(no) > distancia){
					farthest = no;
					distancia = dep.distancia(no);
				}
			}
		}
		return farthest;
	}


	//acha n� mais proximo de um geometrical center
	private static No findClosest(No gc){
		int distancia = Integer.MAX_VALUE;
		No closest = null;
		for (No no : mapa) {
			if(!no.isVisitado()){
				if(gc.distancia(no) <= distancia){
					closest = no;
					distancia = gc.distancia(no);
				}
			}
		}
		return closest;
	}


	private static boolean loop(){
		for (No no : mapa) {
			if(!no.isVisitado()){
				return true;
			}
		}
		return false;
	}


}
import java.util.ArrayList;

public class Cluster {
	private int id;
	private int capacidade;
	private ArrayList<No> caminho;
	private No deposito;
	private No gc; //cluster geometric center
	

	public int getCapacidade() {
		return capacidade;
	}


	public void setCapacidade(int capacidade) {
		this.capacidade = capacidade;
	}


	public ArrayList<No> getCaminho() {
		return caminho;
	}


	public void setCaminho(ArrayList<No> caminho) {
		this.caminho = caminho;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public No getDeposito() {
		return deposito;
	}


	public void setDeposito(No deposito) {
		this.deposito = deposito;
	}


	public No getGc() {
		return gc;
	}


	public void setGc(No gc) {
		this.gc = gc;
	}


	public Cluster(int id, int c, No deposito) {
		this.id = id;
		this.capacidade = c;
		this.deposito = deposito;
		this.caminho = new ArrayList<>();
	}


	public void addNo(No n) {
		this.capacidade -= n.getDemanda();
		n.setVisitado(true);
		this.caminho.add(n);
		this.setGC();
	}

	public void removeNo(No n) {
		this.capacidade += n.getDemanda();
		n.setVisitado(false);
		this.caminho.remove(n);
		this.setGC();
	}

	public int getCusto() {
		int soma = this.caminho.get(0).distancia(this.deposito);
		for (int i = 0; i < this.caminho.size() - 1; i++) {
			soma += this.caminho.get(i).distancia(this.caminho.get(i + 1));
		}
		soma += this.caminho.get(this.caminho.size() - 1).distancia(deposito);
		return soma;
	}
	
	
	public void setGC(){
		this.gc = new No();
		int gcx = 0, gcy = 0;
		for (No no : caminho) {
			gcx +=no.getX();
			gcy+=no.getY();
		}
		gcx/=caminho.size();
		gcy/=caminho.size();
		
		this.gc.setX(gcx);
		this.gc.setY(gcy);
	}
	
	public void print(){
		System.out.print("Cluster " + this.id + ": ");
		System.out.print(this.deposito.getPrintId() + " ");
		for (No no : caminho) {
			System.out.print(no.getPrintId() + " ");
		}
		System.out.print(this.deposito.getPrintId());
		System.out.println();
	}
	
}

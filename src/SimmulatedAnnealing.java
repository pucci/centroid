import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class SimmulatedAnnealing {
	
	private double acceptanceProbability(int energy, int newEnergy, double temperature){
		//se solucao � melhor aceita
		if(newEnergy < energy)
			return 1.0;
		//se a solu��o � pior, calcula chance de aceitar
		return Math.exp((energy-newEnergy)/temperature);
	}
	
	public ArrayList<No> resolve(ArrayList<No> nos){
		//define temperatura e coooling rate pro simmulated annealing
		double temp = 1000;
		double coolingrate = 0.003;
		
		//gera primeira solu��o
		ArrayList<No> solucao = new ArrayList<No>(nos);
		ArrayList<No> melhorsolucao = new ArrayList<No>(solucao);
		
		while(temp > 1){
			//gera novas solu��es trocando 2 n�s de lugar
			ArrayList<No> novasolucao = new ArrayList<No>(solucao);
			Random rng = new Random();
			int rng1 = rng.nextInt(novasolucao.size()-1)+1;
			int rng2 = rng.nextInt(novasolucao.size()-1)+1;
			Collections.swap(novasolucao, rng1, rng2);
			
			//verifica distancias e se aceita novo vizinho
			int currentenergy = getDistance(solucao);
			int newenergy = getDistance(novasolucao);
			if(acceptanceProbability(currentenergy, newenergy, temp)>Math.random()){
				solucao = new ArrayList<No>(novasolucao);
			}
			
			//caso nova solucao seja melhor que a atual
			if(getDistance(solucao)<getDistance(melhorsolucao)){
				melhorsolucao = new ArrayList<No>(solucao);
			}
			
			temp *= 1-coolingrate;
		}
		
		
		return melhorsolucao;
		
	}
	
	
	//calcula custo/distancia de uma solu��o
	private int getDistance(ArrayList<No> nos){
		int distancia = 0;
		for (int i = 0; i < nos.size()-1; i++) {
			distancia += nos.get(i).distancia(nos.get(i+1));
		}
		distancia += nos.get(nos.size()-1).distancia(nos.get(0));
		return distancia;
	}
	
}

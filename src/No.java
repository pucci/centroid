public class No {
	private int id = 0, x = 0, y = 0, demanda = 0;
	private boolean visitado = false;
	
	public No(){
		
	}

	public No(int id, int x, int y, int d) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.setDemanda(d);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isVisitado() {
		return visitado;
	}

	public void setVisitado(boolean visitado) {
		this.visitado = visitado;
	}

	public int distancia(No n) {
		int xd = this.x - n.x;
		int yd = this.y - n.y;
		return (int) Math.round(Math.sqrt(xd * xd + yd * yd));
	}

	public int getDemanda() {
		return demanda;
	}

	public void setDemanda(int demanda) {
		this.demanda = demanda;
	}
	
	public int getPrintId(){
		return id+1;
	}
}
